
#include <stdlib.h>
#include "rlxutils.h"

int main(int argc, char **argv) {
    struct timespec tsi, tsf;

    long const SIZE = 10*1024*1024;

    char   *array_char   = malloc(SIZE*sizeof(char));
    int    *array_int    = malloc(SIZE*sizeof(int));
    long   *array_long   = malloc(SIZE*sizeof(long));
    double *array_double = malloc(SIZE*sizeof(double));
    long double *array_long_double = malloc(SIZE*sizeof(long double));

    printf("size of char         %lu\n", sizeof(char));
    printf("size of int          %lu\n", sizeof(int));
    printf("size of long         %lu\n", sizeof(long));
    printf("size of double       %lu\n", sizeof(double));
    printf("size of float        %lu\n", sizeof(float));
    printf("size of long double  %lu\n", sizeof(long double));

    int i,j, g;
    for (i = 0; i < SIZE; i++) {
        array_int[i] = 3;
        array_char[i] = 3;
        array_long[i] = 3;
        array_double[i] = 3;
        array_long_double[i] = 3;
    }

    char *fname = "cache_line.data";
    printf ("writing to file %s\n", fname);
    FILE *f = fopen(fname, "w");
    fprintf(f, "step_size elapsed_time_char elapsed_time_int elapsed_time_long elapsed_time_double elapsed_time_long_double\n");

    int k[13] = {1,2,4,8,16,32,64,128,256,512,1024, 2048, 4096};
    int N = 10;
    for (j=0; j<13; j++) {

        int step = k[j];
        double time_char = 0;
        double time_int  = 0;
        double time_long = 0;
        double time_double = 0;
        double time_long_double = 0;

        current_utc_time(&tsi);
        for (g = 0; g<N; g++) for (i = 0; i < SIZE; i+=step) array_char[i] *= 3;    
        current_utc_time(&tsf);
        time_char = get_time_diff(tsi, tsf);

        //  =========== MODIFICA DESDE AQUI ============
        // mide los tiempos de ejecucion con los tipos int, long, double y long double
        // y almacenalos en las variables time_int, time_long, time_double y time_long_double 
        // de forma analoga a como se hace con la variable `time_char`
        //  =========== MODIFICA HASTA AQUI ============

        fprintf(f,"%u %lf %lf %lf %lf %lf\n", step, time_char, time_int, time_long, time_double, time_long_double);
    }
    fclose(f);
    return 0; 

}