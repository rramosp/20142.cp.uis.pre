# **Computación Paralela** 2014 - Semestre II

## Pregrado para Ciencias e Ingenierías - Universidad Industrial de Santander

Raúl Ramos-Pollán, Universidad Industrial de Santander, \[[+info](https://sites.google.com/site/rulixrp/courses)\] [rramosp@uis.edu.co](mailto:rramosp@uis.edu.co)

---

Crea una copia local de este repositorio para trabajar en los ejercicios.

Puedes revisar en línea el contenido de los ejercicios en los siguientes enlaces:

### Lección 1 - Introducción
+ [Lección 1 - C warmup](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-01-intro/0.C-warmup/C%20warmup.ipynb)
+ [Lección 1 - Alineamiento de Datos - parte 1](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-01-intro/1.data-alignment/alineamiento%20de%20datos%20-%20parte%201.ipynb)
+ [Lección 1 - Alineamiento de Datos - parte 2](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-01-intro/1.data-alignment/alineamiento%20de%20datos%20-%20parte%202.ipynb)
+ [Lección 1 - Líneas de Caché](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-01-intro/2.cache-lines/lineas%20de%20cache.ipynb)
+ [Lección 1 - Tamaños de Caché](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-01-intro/3.cache-sizes/tamanos%20de%20cache.ipynb)

### Lección 2 - OpenMP
+ [Lección 2 - OpenMP warmup](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-02-openmp/1.omp-warmup/OMP%20warmup.ipynb)
+ [Lección 2 - PI paralelo](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-02-openmp/2.pi-paralelo/PI%20paralelo.ipynb)
+ [Lección 2 - PI mejorado](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-02-openmp/3.pi-mejorado/PI%20mejorado.ipynb)
+ [Lección 2 - Loops](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-02-openmp/4.loops/Loops.ipynb)
+ [Lección 2 - Reducers](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-02-openmp/4.loops/Reduce.ipynb)
+ [Lección 2 - Montecarlo](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-02-openmp/5.montecarlo/Montecarlo%20PI.ipynb)

### Lección 3 Python y MPI

[Experimento MPI con cluster de máquinas virtuales en el laboratorio](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-03-mpi/MPI-cluster-test.ipynb)

+ [Lección 3 - Python vectorización](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-03-mpi/3.1%20vectorizacio%CC%81n%20python.ipynb)
+ [Lección 3 - Python partición de vectores](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-03-mpi/3.2%20particionamiento%20vectores%20python.ipynb)
+ [Lección 3 - MPI básico](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-03-mpi/3.3%20MPI-hello.ipynb)
+ [Lección 3 - MPI vector](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-03-mpi/3.4%20MPI%20vector.ipynb)

### Lección 4 CUDA

[Introducción a CUDA con Python](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-04-cuda/CUDA%20Intro.ipynb)

+ [Lección 4 - Convolución simple](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-04-cuda/4.1%20CUDA%20simple%20covolution.ipynb)
+ [Lección 4 - Operaciones múltiples](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/20142.cp.uis.pre/raw/master/leccion-04-cuda/4.2%20CUDA%20multiple%20operations.ipynb)
