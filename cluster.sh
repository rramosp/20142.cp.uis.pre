#!/bin/sh

function check_unreachable() {

  local node=$1
  wget --timeout=0.1 $node:9999 -t 1 2> /tmp/err
  grep refused /tmp/err > /dev/null
  local r=$?
  echo "$r"
}

arg=$1
netbackupfile="netconfig.backup"

ids="10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29"

if [[ -z "$arg" ]]
then
    echo "error: must call with -restore or -setup or -updatekeys"
    exit 1
fi

if [ $arg == "-restore" ]
then 
   echo "restoring network configuration from backup at $netbackupfile ..."
   if [ ! -f "$netbackupfile" ]
   then 
      echo "error: backup file $netbackupfile does not exist"
      exit -1
   fi
   sudo cp "$netbackupfile.hosts" /etc/hosts
   sudo hostname `cat "$netbackupfile.hostname"`
   sudo system-config-network-cmd -i -c -f $netbackupfile
   sudo service network restart
   sudo service NetworkManager start
   sudo chkconfig NetworkManager on
   sudo service iptables start
   sudo chkconfig iptables on
   mv -f $netbackupfile "$netbackupfile.last_restored"
   exit 0
fi

if [ $arg == "-updatekeys" ]
then
  if [ -f $HOME/.ssh/known_hosts ]; then
     rm $HOME/.ssh/known_hosts
  fi

  for i in $ids
  do
    unreachable=$(check_unreachable "node$i")
    if [ $unreachable == 0 ]; then
      ssh-keyscan "node$i" >> $HOME/.ssh/known_hosts 2>/dev/null
      sshpass -p user ssh-copy-id "node$i" > /dev/null 2> /dev/null
      echo "node$i enabled"
    fi
  done
  ssh-add

  exit 0
fi

if [ $arg != "-setup" ]
then 
   echo "error: incorrect parameter $arg must be -restore or -setup or -updatekeys"
   exit 1
fi

nodeid=$2
if [[ -z "$nodeid" ]]
then
    echo "error: no node name specified, must call -setup nodeid"
    exit 1
fi

nodename="node$nodeid"

re='^[1-3][0-9]$'
if ! [[ $nodeid =~ $re ]] ; then
   echo "error: id must be a two digit number greater than between 10 and 39"
   exit 1
fi

echo "reconfiguring using node name $nodename ....."

sudo yum install moreutils iftop -y
#sudo yum install iftop -y

if [ -f "$netbackupfile" ]
then
    echo "previous network configuration backup file exists"
    echo "you must restore to reconfigure network for MPI cluster"
    exit -1
fi

device=`iftop -s 1 -t 2>&1 >/dev/null| grep interface | awk '{ print $2 }'`
ip=`ifdata -pa $device`
mac=`ifdata -ph $device`
echo "============= device information ======="
echo $device $ip $mac
echo "========================================"

# --------------------------------
# backs up network configuration
# --------------------------------
sudo system-config-network-cmd > $netbackupfile
sudo hostname > "$netbackupfile.hostname"
sudo cp /etc/hosts "$netbackupfile.hosts"
# --------------------------------

# --------------------------------
# reconfigures with static ip
# --------------------------------
echo "reconfiguring ip"
baseip="192.168.222"
ifcfgfile="/etc/sysconfig/network-scripts/ifcfg-$device"
echo "writing static network config to $ifcfgfile"

/bin/cat <<EOM > /tmp/ifcfg
DEVICE=$device
BOOTPROTO=static
IPADDR="$baseip.$nodeid"
NETMASK=255.255.0.0
HWADDR=$mac
NM_CONTROLLED=no
ARPCHECK=no
ONBOOT=yes
TYPE=Ethernet
IPV6INIT=no
EOM

sudo mv /tmp/ifcfg $ifcfgfile
# --------------------------------

# --------------------------------
# reconfigures hostname and hosts file
# --------------------------------
echo "reconfiguring hostname to $nodename"
sudo hostname $nodename
hostname

echo "reconfiguring /etc/hosts"
if [ -f "/tmp/hosts" ] ; then
   rm /tmp/hosts
fi

for i in $ids
do
   echo "$baseip.$i node$i" >> /tmp/hosts
done
sudo mv /tmp/hosts /etc/hosts

sudo service NetworkManager stop
sudo chkconfig NetworkManager off
sudo service iptables stop
sudo chkconfig iptables off
sudo service network restart

# --------------------------------
# generate new ssh key
# --------------------------------
echo "generating ssh key in $HOME/.ssh"
if [ ! -d "$HOME/.ssh" ] ; then
   echo "creating .ssh folder"
   mkdir $HOME/.ssh
fi

rm -rf $HOME/.ssh/*

rm $HOME/.ssh/id_rsa*
ssh-keygen -t rsa -C "" -N "" -f $HOME/.ssh/id_rsa

echo "========================================"
echo "node reconfigured as static: $nodeip $nodename "

