#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "image.c"

int main(int argc, char **argv)
{
        if (argc != 5)
                abort_("Usage: program_name <img_in> <img_walls> <img_out> <ncycles>");
	char *num;
	long ncycles = strtol(argv[4], &num, 10);

	printf ("using %d cycles\n", ncycles);

	float *img;
	int iw,ih;

	float *walls;
	int ww,wh;

	float *tmp;

        long i,j, k;

        read_png_file(argv[1], &img, &iw, &ih);
        read_png_file(argv[2], &walls, &ww, &wh);

	float r = 0.1;
	int x,y;
	for (y=1; y<ih-1; y++) {
	  for(x=1; x<iw-1; x++) {
	    i = x+iw*y;
	    if (walls[i]==0.0) img[i]=1-img[i];
	  }
	}

        write_png_file(argv[3], img, iw, ih);

        return 0;
}

