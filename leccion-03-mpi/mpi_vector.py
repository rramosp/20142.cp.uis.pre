from mpi4py import MPI
import numpy as np
from time import time, sleep

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
ntasks = comm.Get_size()

n = 10010
v = None

def vector_sum(v):
    sleep(2)
    return np.sum(v)

if rank==0:
    time_start = time()
    v = np.random.randint(10, size=n)
    elems_per_task = n/ntasks
    total = vector_sum(v[0:elems_per_task])
    for i in range(1,ntasks):
        vsize  = elems_per_task if i<ntasks-1 else n - elems_per_task*(ntasks-1)
        vstart = elems_per_task*i
        vend   = vstart+vsize
        comm.send(v[vstart:vend], dest=i)
    for i in range(1,ntasks):
        task_subtotal = comm.recv(source=i)
        total += task_subtotal
    print "elapsed time ", time()-time_start
    print "distributed total", total
    print "computed    total", np.sum(v)
        
else:
    my_part = comm.recv(source=0)
    print "maquina", MPI.Get_processor_name(), "recibe vector de", len(my_part), "elementos"
    comm.send(vector_sum(my_part), dest=0)
        