from mpi4py import MPI
import numpy as np
from time import time,sleep
import socket

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
ntasks = comm.Get_size()
t_start = time();

if rank == 0:
    r = np.random.randint(10)
    total = 0
    print "this is MASTER running on", MPI.Get_processor_name()
    for i in range(1,ntasks):
        r = comm.recv(source=i)
        print "rank",i,"said", r
    print "elapsed time", time()-t_start, "total", total
else:
    comm.send(MPI.Get_processor_name(), dest=0)